import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NemModule } from './nem/nem.module';
import { PaymentsComponent } from './nem/payments/payments.component';
import { DonateComponent } from './nem/donate/donate.component';

const appRoutes: Routes = [
  { path: 'pay', component: PaymentsComponent },
  { path: 'thanks', component: DonateComponent },
  { path: '**',  redirectTo: '/pay',  pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes,
      { enableTracing: false,  // true for debugging purposes only
        useHash: true,         // using hash location strategy because cannot serve app on every url
       } 
    ),
    BrowserModule,
    HttpClientModule,
    NemModule
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
