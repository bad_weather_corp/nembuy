import { Component, OnInit } from '@angular/core';
import { NemService } from '../services/nem.service';

@Component({
  selector: 'app-nem-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {
  address: string;
  currency: string;

  constructor() {
        // default address is auto-saved
        let defaultAddress = PaymentsComponent.parseCookieValue("address");
        if (defaultAddress === null || defaultAddress.length == 0) {
          defaultAddress = 'NAQFKQ-GP5CH3-4M5MM5-GAPW3F-67GOYO-GBUKWA-AGII';
        }
        this.address = defaultAddress;
        // default currency is saved
        let defaultCurrency = PaymentsComponent.parseCookieValue("currency");
        if (defaultCurrency == null || defaultCurrency.length == 0) {
          defaultCurrency = "nem:xem";
        }
        this.currency = defaultCurrency;
  }

  ngOnInit() {

  }

  static parseCookieValue(name: string): string {
    const cookies = document.cookie
      .split(';')
      .filter(item => { return item.trim().startsWith(name) })
      .map(propVal => { return propVal.split('=') });
    // return the value
    if (cookies.length > 0 && cookies[0].length > 1) {
      return cookies[0][1].trim();
    } else {
      return null;
    }
  }

}
