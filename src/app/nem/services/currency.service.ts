import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient} from "@angular/common/http";
import { CoinmarketcapConvert } from '../model/coinmarketcap.convert';

@Injectable()
export class CurrencyService {

  private czkXem: number = 1;
  private eurXem: number = 1;
  private jpyXem: number = 1;
  private usdXem: number = 1;

  constructor(private http: HttpClient) {
    this.monitorPrice('CZK').subscribe(convert => this.czkXem = CoinmarketcapConvert.getPrice(convert, 'CZK'));
    this.monitorPrice('EUR').subscribe(convert => this.eurXem = CoinmarketcapConvert.getPrice(convert, 'EUR'));
    this.monitorPrice('JPY').subscribe(convert => this.jpyXem = CoinmarketcapConvert.getPrice(convert, 'JPY'));
    this.monitorPrice('USD').subscribe(convert => this.usdXem = CoinmarketcapConvert.getPrice(convert, 'USD'));
  }

  /**
   * 
   * @param currency currency to retrieve the value of
   */
  monitorPrice(currency: string): Observable<CoinmarketcapConvert> {
    // use XEM ticker (873) to get values for various currencies
    const url = CoinmarketcapConvert.getCurrencyURL(currency);
    console.log('Query for exchange rate '+url);
    return this.http.get<CoinmarketcapConvert>(url);
  }

  public convertToMicroXemFixedRate(rate: number, value: number) {
    let amount = value / rate;
    // convert to microxem while having precision of 3 decimal places
    return Math.round(amount * 1000)*1000;
  }

  public getRate(currency: string) {
    if (currency === "nem:xem") {
      return 1;
    } else if (currency === "fiat:czk") {
      return this.czkXem;
    } else if (currency === "fiat:eur") {
      return this.eurXem;
    } else if (currency === "fiat:jpy") {
      return this.jpyXem;
    } else if (currency === "fiat:usd") {
      return this.usdXem;
    } else {
      // weird, just use the value
      return 1;
    }
  }
}
