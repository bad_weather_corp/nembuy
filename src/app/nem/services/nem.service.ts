import { Injectable } from '@angular/core';
import { NEMLibrary, NetworkTypes, Address, AccountHttp, Pageable, Transaction, ChainHttp, BlockchainListener, ConfirmedTransactionListener, UnconfirmedTransactionListener } from 'nem-library';
import { Observable, BehaviorSubject } from 'rxjs';
import { WebSocketConfig } from 'nem-library/dist/src/infrastructure/Listener';

@Injectable()
export class NemService {

  /** nem nodes used for the access to the blockchain */
  private static nodes: WebSocketConfig[] = [
        //  {protocol: "http", domain: "192.3.61.243", port: 7778},
        //  {protocol: "http", domain: "192.3.61.243",  port: 7778}
        ];

  // observable for block height
  private _blockHeight$ = new BehaviorSubject<number>(0);
  public readonly blockHeight$ = this._blockHeight$.asObservable();
  // observable for transactions
  private confirmedTransactionsListener: Observable<Transaction>;
  private unConfirmedTransactionsListener: Observable<Transaction>;

  /**
   * create new instance of the service
   */
  constructor() { }

  /**
   * initialize nem library to specific network
   */
  init(): Promise<any> {
    return new Promise((resolve, reject) => {
      console.log('initializing nem library');
      // pick NetworkTypes.MAIN_NET or TEST_NET
      const networkType = NetworkTypes.MAIN_NET;
      // initialize the library
      NEMLibrary.bootstrap(networkType);
      // make sure we really selected desired network
      if (NEMLibrary.getNetworkType() === networkType) {
        console.log('successfully initialized NEM library. Creating listeners');
        this.subscribeToBlockHeightListener();
        // resolve the promise
        resolve();
      } else {
        reject();
      }
    });
  }

  /**
   * load initial value for block height and subscribe to updates
   */
  subscribeToBlockHeightListener(): void {
    // load initial value
    new ChainHttp().getBlockchainHeight().subscribe(
      height => this._blockHeight$.next(height),
      err => this._blockHeight$.error(err)
    );
    // subscribe for updates
    new BlockchainListener().newHeight().subscribe(
      height => this._blockHeight$.next(height),
      err => this._blockHeight$.error(err)
    )
  }

  /**
   * get listener for the confirmed transactions
   */
  getConfirmedTransactionsListener(address: string): Observable<Transaction> {
    if (this.confirmedTransactionsListener == null) {
      const addr = new Address(address);
      this.confirmedTransactionsListener = new ConfirmedTransactionListener().given(addr);
    }
    return this.confirmedTransactionsListener;
  }

  /**
   * get listener for unconfirmed transactions - NOTE THAT THIS DOES NOT WORK
   */
  getUnconfirmedTransactionsListener(address: string): Observable<Transaction> {
    if (this.unConfirmedTransactionsListener == null) {
      const addr = new Address(address);
      this.unConfirmedTransactionsListener = new UnconfirmedTransactionListener().given(addr);
    }
    return this.unConfirmedTransactionsListener;
  }

  /**
   * get pageable list of transactions
   * 
   * @param address 
   */
  public loadTransactions(address: string): Pageable<Transaction[]> {
    const addr = new Address(address);
    console.log('loading transactions for address '+ addr);
    const accountHttp = new AccountHttp();
    return accountHttp.incomingTransactionsPaginated(addr, {pageSize: 20});
  }
}
