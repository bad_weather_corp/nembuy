import { TestBed, inject } from '@angular/core/testing';

import { NemService } from './nem.service';

describe('NemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NemService]
    });
  });

  it('should be created', inject([NemService], (service: NemService) => {
    expect(service).toBeTruthy();
  }));
});
