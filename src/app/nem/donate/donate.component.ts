import { Component, OnInit, OnDestroy } from '@angular/core';
import { Invoice } from '../model/invoice';
import { CurrencyService } from '../currency.service';
import { Subscription } from 'rxjs';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { CoinmarketcapConvert } from '../model/coinmarketcap.convert';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.css']
})
export class DonateComponent implements OnInit, OnDestroy {
  invoice: Invoice = new Invoice();
  message: String;
  rate: number;
  rate$: Subscription;

  constructor(private svcCurrency: CurrencyService, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    // this.invoice.address = 'NAQFKQ-GP5CH3-4M5MM5-GAPW3F-67GOYO-GBUKWA-AGII';
    // this.invoice.amount = 10;
    // this.invoice.currency = 'nem:xem';
    // this.processRate(this.invoice.currency);
    this.activatedRoute.queryParams.subscribe(params => {
      this.invoice.address = params['address'];
      this.invoice.amount = params['amount'];
      this.invoice.currency = params['currency'];
      this.message = params['message'];
      this.processRate(this.invoice.currency);
    });
  }

  ngOnDestroy() {
    if (this.rate$ != null) {
      this.rate$.unsubscribe();
    }
  }

  processRate(currency: string) {
    // first unsubscribe
    if (this.rate$ != null) {
      this.rate$.unsubscribe();
    }
    // monitor exchange rate every 30s
    this.rate$ = TimerObservable
      .create(0, 30000)
      .subscribe(() => {
        this.svcCurrency.monitorPrice(currency)
          .subscribe(convert => {
            console.log(convert);
            this.rate = CoinmarketcapConvert.getPrice(convert, currency);
          });
      });
  }

  onChangeCurrency(newCurrency: string) {
    this.processRate(newCurrency);
  }
}
