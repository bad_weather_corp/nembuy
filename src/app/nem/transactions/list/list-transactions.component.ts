import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Transaction } from 'nem-library';
import { NemService } from '../../services/nem.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list-transactions',
  templateUrl: './list-transactions.component.html',
  styleUrls: ['./list-transactions.component.css']
})
export class ListTransactionsComponent implements OnInit, OnDestroy {
  nem: NemService;
  transactions: Transaction[] = [];
  blockHeight$: Observable<number>;
  @Input() address: string;

  constructor(nem: NemService) {
    this.nem = nem;
    this.blockHeight$ = this.nem.blockHeight$;
  }

  ngOnInit() {
    // load initial data
    this.nem.loadTransactions(this.address).subscribe(loadedTs => {
      loadedTs.forEach(trans => {
        this.processTransaction(trans, false);
      });
      this.transactions.sort(ListTransactionsComponent.compareTransactionsInversed);
    });
    // subscribe to new confirmed transactions
    this.nem.getConfirmedTransactionsListener(this.address).subscribe(
      trans => {
        console.log('notify confirmed transaction ' + trans.getTransactionInfo().hash.data);
        this.processTransaction(trans, true) 
      },
      err => {
        console.log('Failed to get new unconfirmed transaction ' + err);
      });
    // subscribe to unconfirmed transactions
    this.nem.getUnconfirmedTransactionsListener(this.address).subscribe(
      trans => {
        console.log('notify unconfirmed transaction ' + trans.getTransactionInfo().hash.data);
        this.processTransaction(trans, true) 
      },
      err => {
        console.log('Failed to get new unconfirmed transaction ' + err);
      });
  }

  ngOnDestroy() {
    // TODO unsubscribe from observables
  }

  private processTransaction(trans: Transaction, doSort = true) {
    console.log('component processing transaction ' + trans.getTransactionInfo().hash.data);
    // add the invoice to the array if it is not there already
    var found = false;
    this.transactions.forEach((t, index) => {
      if (t.getTransactionInfo().id === trans.getTransactionInfo().id) {
        found = true;
        console.log("updating transaction " + trans.getTransactionInfo().hash.data);
        this.transactions[index] = trans;
      }
    });
    if (!found) {
      console.log("adding new transaction " + trans.getTransactionInfo().hash.data);
      this.transactions.push(trans);
    }
    // sort the transactions
    if (doSort) {
      this.transactions.sort(ListTransactionsComponent.compareTransactionsInversed);
    }
  }

  /**
   * compare transactions in ascending order - i.e. the order they were created (oldest first)
   * 
   * @param a 
   * @param b 
   */
  private static compareTransactions(a: Transaction, b: Transaction): number {
    if (a.isConfirmed()) {
      if (!b.isConfirmed()) {
        // A is already confirmed in a block but B is not
        return 1;
      } else {
        // both are in a block so lets see
        var blockDiff = a.getTransactionInfo().height - b.getTransactionInfo().height;
        if (blockDiff === 0) {
          // same block so compare timestamps
          return a.timeWindow.timeStamp.compareTo(b.timeWindow.timeStamp);
        } else {
          // different block so use that as ordering
          return blockDiff;
        }
      }
    } else {
      if (b.isConfirmed()) {
        // B confirmed and A not so B is earlier
        return -1;
      } else {
        // both unconfirmed so compare timestamps
        return a.timeWindow.timeStamp.compareTo(b.timeWindow.timeStamp);
      }
    }
  }

  /**
   * compare transactions in inversed order - i.e. newest to oldest
   * 
   * @param a 
   * @param b 
   */
  private static compareTransactionsInversed(a: Transaction, b: Transaction): number {
    return -ListTransactionsComponent.compareTransactions(a, b);
  }
}
