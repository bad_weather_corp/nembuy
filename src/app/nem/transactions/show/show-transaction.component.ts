import { Component, OnInit, Input } from '@angular/core';
import { Transaction, MultisigTransaction, TransferTransaction } from 'nem-library';


@Component({
  selector: 'app-show-transaction',
  templateUrl: './show-transaction.component.html',
  styleUrls: ['./show-transaction.component.css']
})
export class ShowTransactionComponent implements OnInit {
  @Input() transaction: Transaction;
  @Input() currentHeight: number;

  transactionAmount: number;
  transactionHeight: number;
  confirmed: boolean;

  constructor() { }

  ngOnInit() {
    console.log("displaying transaction "+this.transaction.getTransactionInfo().hash.data);
    var t = this.transaction;
    if (this.transaction instanceof MultisigTransaction) {
      t = this.transaction.otherTransaction;
    }
    if (t instanceof TransferTransaction) {
      this.transactionAmount = t.xem().amount;
    }
    this.transactionHeight = t.getTransactionInfo().height;
    this.confirmed = t.isConfirmed();
  }

}
