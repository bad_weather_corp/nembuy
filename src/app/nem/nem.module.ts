import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NemService } from './services/nem.service';
import { QRCodeModule } from 'angularx-qrcode';
import { QRService } from 'nem-library';
import { CreateInvoiceComponent } from './invoices/create/create-invoice.component';
import { ListTransactionsComponent } from './transactions/list/list-transactions.component';
import { ShowTransactionComponent } from './transactions/show/show-transaction.component';
import { PaymentsComponent } from './payments/payments.component';
import { QRCodePipe } from './pipes/qrcode.pipe';
import { DurationPipe } from './pipes/duration.pipe';
import { CurrencyService } from './services/currency.service';
import { AmountPipe } from './pipes/amount.pipe';
import { RatePipe } from './pipes/rate.pipe';
import { DonateComponent } from './donate/donate.component';

/**
 * call to initialize the NEM service and start refresh of data
 * @param service 
 */
export function init_nemservice(service: NemService) {
  return () => service.init();
}

@NgModule({
  imports: [
    CommonModule,
    QRCodeModule,
    FormsModule
  ],
  declarations: [
    PaymentsComponent,
    ShowTransactionComponent,
    CreateInvoiceComponent,
    ListTransactionsComponent,
    DurationPipe,
    QRCodePipe,
    AmountPipe,
    RatePipe,
    DonateComponent],
  exports: [
    PaymentsComponent
  ],
  providers: [
    // include the nem service
    NemService,
    CurrencyService,
    QRService,
    // initialize nem service when app starts
    { provide: APP_INITIALIZER, useFactory: init_nemservice, deps: [NemService], multi: true },
]
})
export class NemModule { }
