
export class CoinmarketcapConvert {
    data: {
        id: number;
        name: string;
        symbol: string;
        website_slug: string;
        rank: string;
        circulating_supply: string;
        total_supply: string;
        max_supply: string;
        quotes: Object;
    };
    metadata: {
        timestamp: number;
        error: string;
    }

    static getPrice(convert: CoinmarketcapConvert, currency: string): number {
        return convert.data.quotes[CoinmarketcapConvert.getCurrencyCode(currency)]['price'];
    }

    static getCurrencyURL(currency: string): string {
        return "https://api.coinmarketcap.com/v2/ticker/873/?convert="+CoinmarketcapConvert.getCurrencyCode(currency);
    }

    static getCurrencyCode(currencyId: string): string {
        switch (currencyId) {
            case 'fiat:czk': return 'CZK';
            case 'fiat:eur': return 'EUR';
            case 'fiat:usd': return 'USD';
            case 'fiat:jpy': return 'JPY';
            case 'nem:xem': return 'XEM';
            default: return currencyId;
        }
    }
}

/*
{
    "data": {
        "id": 873, 
        "name": "NEM", 
        "symbol": "XEM", 
        "website_slug": "nem", 
        "rank": 17, 
        "circulating_supply": 8999999999.0, 
        "total_supply": 8999999999.0, 
        "max_supply": null, 
        "quotes": {
            "CZK": {
                "price": 3.1586352637, 
                "volume_24h": 175281575.40535617, 
                "market_cap": 28427717371.0, 
                "percent_change_1h": 0.22, 
                "percent_change_24h": -2.48, 
                "percent_change_7d": -17.0
            }, 
            "USD": {
                "price": 0.1424538722, 
                "volume_24h": 7905166.96449937, 
                "market_cap": 1282084850.0, 
                "percent_change_1h": 0.22, 
                "percent_change_24h": -2.48, 
                "percent_change_7d": -17.0
            }
        }, 
        "last_updated": 1533583888
    }, 
    "metadata": {
        "timestamp": 1533583473, 
        "error": null
    }
}
*/