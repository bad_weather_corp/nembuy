import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { CurrencyService } from '../../services/currency.service';
import { CoinmarketcapConvert } from '../../model/coinmarketcap.convert';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Subscription } from 'rxjs';
import { Invoice } from '../../model/invoice';

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.css']
})
export class CreateInvoiceComponent implements OnInit, OnDestroy {
  invoice: Invoice = new Invoice();
  @Input() address: string;
  @Input() currency: string;
  rate: number;
  rate$: Subscription;
  svcCurrency: CurrencyService;

  constructor(svcCurrency: CurrencyService) {
    this.svcCurrency = svcCurrency;
  }

  ngOnInit() {
    // default amount is always 0
    this.invoice.amount = 0;
    // retrieve address and currency from the parent
    this.invoice.address = this.address;
    this.invoice.currency = this.currency;
    // use nem:xem as mosaic
    this.invoice.mosaic = "nem:xem";
    // initialize rate monitoring
    this.processRate(this.currency);
  }

  ngOnDestroy() {
    if (this.rate$ != null) {
      this.rate$.unsubscribe();
    }
  }

  processRate(currency: string) {
    // first unsubscribe
    if (this.rate$ != null) {
      this.rate$.unsubscribe();
    }
    // monitor exchange rate every 30s
    this.rate$ = TimerObservable
      .create(0, 30000)
      .subscribe(() => {
        this.svcCurrency.monitorPrice(currency)
          .subscribe(convert => {
            console.log(convert);
            this.rate = CoinmarketcapConvert.getPrice(convert, currency);
          });
      });
  }

  onChangeAddress(newAddress: string){
    CreateInvoiceComponent.setCookieValue("address", newAddress);
  }

  onChangeCurrency(newCurrency: string){
    CreateInvoiceComponent.setCookieValue("currency", newCurrency);
    this.processRate(newCurrency);
  }

  static setCookieValue(name: string, value: string) {
    // set the value
    document.cookie = [name, value].join('=');
  }
}
