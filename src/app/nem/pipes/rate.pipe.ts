import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyService } from '../services/currency.service';
import { Invoice } from '../model/invoice';

@Pipe({
  name: 'rate',
  pure: false
})
export class RatePipe implements PipeTransform {
  private currencySvc: CurrencyService;

  constructor(currencySvc: CurrencyService) {
    this.currencySvc = currencySvc;
  }
  
  transform(invoice: Invoice, args?: any): any {
    let rate = this.currencySvc.getRate(invoice.currency);
    let fromCurrency = invoice.currency==='nem:xem'?'XEM':invoice.currency.toUpperCase();
    let toCurrency = invoice.mosaic==='nem:xem'?'XEM':invoice.mosaic;
    return rate + ' ' + fromCurrency + '/' + toCurrency;
  }

}
