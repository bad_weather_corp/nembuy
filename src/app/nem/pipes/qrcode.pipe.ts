import { Pipe, PipeTransform } from '@angular/core';
import { Invoice } from '../model/invoice';
import { QRService, Address } from 'nem-library';
import { CurrencyService } from '../services/currency.service';

@Pipe({
  name: 'qrcode',
  pure: false
})
export class QRCodePipe implements PipeTransform {
  private qrService: QRService;
  private currencySvc: CurrencyService;

  constructor(qrService: QRService, currencySvc: CurrencyService) {
    this.qrService = qrService;
    this.currencySvc = currencySvc;
  }

  transform(invoice: Invoice, rate: number): string {
    // make the conversion to microXem
    const amount = this.currencySvc.convertToMicroXemFixedRate(rate, invoice.amount);
    // generate qr code
    let code = this.qrService.generateTransactionQRText(new Address(invoice.address), amount, invoice.message);
    // return the value
    return code;
  }

}
