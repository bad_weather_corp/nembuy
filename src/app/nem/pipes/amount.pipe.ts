import { Pipe, PipeTransform } from '@angular/core';
import { Invoice } from '../model/invoice';
import { CurrencyService } from '../services/currency.service';

@Pipe({
  name: 'amount',
  pure: false
})
export class AmountPipe implements PipeTransform {
  private currencySvc: CurrencyService;

  constructor(currencySvc: CurrencyService) {
    this.currencySvc = currencySvc;
  }
  
  transform(invoice: Invoice, rate: number): any {
    let amount = this.currencySvc.convertToMicroXemFixedRate(rate, invoice.amount);
    let mosaicName = invoice.mosaic;
    // pretty print the XEM
    if (invoice.mosaic === 'nem:xem') {
      mosaicName = "XEM";
      amount = amount / 1000000;
    }
    return amount + ' ' + mosaicName + ' (' + rate + ' ' + invoice.currency.toUpperCase() + '/' + mosaicName+')' ;
  }

}
