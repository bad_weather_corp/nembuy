import { TestBed, inject } from '@angular/core/testing';
import { QRCodePipe } from './qrcode.pipe';
import { QRService } from 'nem-library';

describe('QRCodePipe', () => {
  it('create an instance', () => {
    let pipe;

    beforeEach(() => TestBed.configureTestingModule({
      providers: [
        QRCodePipe,
        {
          provide: QRService,
          useClass: QRService,
        },
      ],
    }));

    beforeEach(inject([QRCodePipe], p => {
      pipe = p;
    }));
    expect(pipe).toBeTruthy();
  });
});
