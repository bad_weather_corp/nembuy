import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(value: number, args?: any): string {
    const days = Math.floor(value / 60 / 24);
    const hours = Math.floor((value%(60*24)) / 60);
    const minutes = Math.floor(value%60);
    return days+"d "+hours+"h "+minutes+"m";
  }

}
