import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('transTitle') transTitle : ElementRef;
  
  constructor(private titleService: Title) {}
  
  ngOnInit() {
    this.titleService.setTitle(this.transTitle.nativeElement.textContent);
  }
}
