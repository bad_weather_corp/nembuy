# Nembuy

NemBuy allows payments where user selects currency/token to define value in and pays using XEM on nem.io network.

## Exchange rates

Exchange rates are retrieved from coinmarketcap API. Currently supported value entry currencies/tokes are

 - XEM - no conversion is performed
 - CZK - exotic FIAT currency used in Czech republic
 - EUR - Euro used in most of civilized european countries
 - JPY - Japan is big on crypto na nem.io
 - USD - American dolar is omnipresent

## Build the project

 - `npm run-script build` will build english locale
 - `npm run-script buildSK` will build slovak locale
 - `npm run-script buildCZ` will build czech locale

`index.html` in the root project serves as a redirection point to locales. URL fragment is used by angular router so the location hash needs to be preserved

## License

Project is MIT licensed. See the LICENSE file.

## Contribution

All contributions are welcome. Use issue tracker to suggest functionality.